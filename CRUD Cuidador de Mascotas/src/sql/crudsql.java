package sql;


import java.sql.Connection;
import javax.swing.JOptionPane;
import java.sql.ResultSet;
import getset.variables;

public class crudsql extends conexionsql {

    java.sql.Statement st;
    ResultSet rs;
    variables var = new variables();

    public void insertar(String ingreso, String nombre, String pago) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "insert into cuidador(date_ingreso_cuidador,nombre_cuidador,pago_cuidador) values('" + ingreso + "','" + nombre + "','" + pago + "');";
            st.execute(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "El resgistro se guardó correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "El resgistro NO se guardó " + e, "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
    }
        
    public void mostrar(String id_cuidador) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "select * from cuidador where id_cuidador='" + id_cuidador + "';";
            rs = st.executeQuery(sql);
            if (rs.next()) {
                var.setIdcuidador(rs.getString(1));
                var.setIngreso(rs.getString(2));
                var.setNombre(rs.getString(3));
                var.setPago(rs.getString(4));
            } else {
                var.setIdcuidador("");
                var.setIngreso("");
                var.setNombre("");
                var.setPago("");
                JOptionPane.showMessageDialog(null, "No se encontraron datos", "sin registro", JOptionPane.INFORMATION_MESSAGE);
            }
            st.close();
            conexion.close();

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "error en programa " + e, "Erro de sistema", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void actualizar(String ingreso, String pago, String nombre, String idcuidador) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "update cuidador set date_ingreso_cuidador='" + ingreso + "',	\n" + "nombre_cuidador='" + nombre + "',pago_cuidador='" + pago + "' where "
            + "\n" + "id_cuidador='" + idcuidador + "'; ";
            st.executeUpdate(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "El registro se actualizo", "Exito", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al actualizar " + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void eliminar(String idcuidador){
        try {
            Connection conexion=conectar();
            st=conexion.createStatement();
            String sql="delete from cuidador where id_cuidador='"+idcuidador+"'; ";
            st.executeUpdate(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "Registro eliminado correctamente","Eliminado",JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al eliminar registro "+ e,"Error",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void insertar2(String nombreservicio, String costoservicio) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "insert into tipo_servicio(nombre_tipo_servicio,costo_tipo_servicio) values('" + nombreservicio + "','" + costoservicio +"');";
            st.execute(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "El registro de servicio se guardó correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "El resgistro NO se guardó " + e, "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
    }
    public void buscar(String id_servicio) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "select * from tipo_servicio where id_tipo_servicio='" + id_servicio + "';";
            rs = st.executeQuery(sql);
            if (rs.next()) {
                var.setIdservicio2(rs.getString(1));
                var.setTiposervicio2(rs.getString(2));
                var.setCostoservicio2(rs.getString(3));
            } else {
                var.setIdservicio2("");
                var.setTiposervicio2("");
                var.setCostoservicio2("");
                JOptionPane.showMessageDialog(null, "No se encontraron datos", "sin registro", JOptionPane.INFORMATION_MESSAGE);
            }
            st.close();
            conexion.close();

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "error en programa " + e, "Erro de sistema", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void actualizar2(String idservicio, String tiposervicio, String costoservicio) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "update tipo_servicio set id_tipo_servicio='" + idservicio + "',	\n" + "nombre_tipo_servicio='" + tiposervicio + "',costo_tipo_servicio='" + costoservicio + "' where "
            + "\n" + "id_tipo_servicio='" + idservicio + "'; ";
            st.executeUpdate(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "El registro se actualizo", "Exito", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al actualizar " + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    public void eliminar2(String idservicio){
        try {
            Connection conexion=conectar();
            st=conexion.createStatement();
            String sql="delete from tipo_servicio where id_tipo_servicio='"+idservicio+"'; ";
            st.executeUpdate(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "Registro eliminado correctamente","Eliminado",JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al eliminar registro "+ e,"Error",JOptionPane.ERROR_MESSAGE);
        }
    }
}

























































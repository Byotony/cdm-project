package getset;

public class variables {

    private static String idcuidador;
    private static String ingreso;
    private static String pago;
    private static String nombre;
    private static String idservicio2;
    private static String tiposervicio2;
    private static String costoservicio2;

    public String getIdcuidador() {
        return idcuidador;
    }

    public void setIdcuidador(String idcuidador) {
        this.idcuidador = idcuidador;
    }

    public String getIngreso() {
        return ingreso;
    }

    public void setIngreso(String ingreso) {
        this.ingreso = ingreso;
    }

    public String getPago() {
        return pago;
    }

    public void setPago(String pago) {
        this.pago = pago;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdservicio2() {
        return idservicio2;
    }

    public void setIdservicio2(String idservicio2) {
        this.idservicio2 = idservicio2;
    }

    public String getTiposervicio2() {
        return tiposervicio2;
    }

    public void setTiposervicio2(String tiposervicio2) {
        this.tiposervicio2 = tiposervicio2;
    }

    public String getCostoservicio2() {
        return costoservicio2;
    }

    public void setCostoservicio2(String costoservicio2) {
        this.costoservicio2 = costoservicio2;
    }
    
}

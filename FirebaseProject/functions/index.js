const functions = require("firebase-functions");
const admin=require("firebase-admin");
admin.initializeApp();
const db=admin.firestore();

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
exports.onUserCreate = functions.firestore.document('servicio/{servicioId}').onCreate(async (snap, context) => {
    const values = snap.data();
    // Contar matriculas por id de nivel funciona
    const query = db.collection("servicio");
    const snapshot = await query.where("id_cuidador","==",values.id_cuidador ,"n_semana","==",values.n_semana).get();
    const count = snapshot.size;
    /***************/


    /****************/
    //preguntamos en la condicion para eliminar el registro si es el caso
    if(count > 5){
            try {
                const res = await db.collection('servicio').doc(snap.id).delete();
            } catch (error) {
              console.log(error);  
            }
    }
    /*******************/
})
